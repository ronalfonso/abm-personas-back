FROM openjdk:17-alpine as maven

WORKDIR /app

ARG JAR_FILE=target/abm-persona-app-dev.jar

#COPY ${JAR_FILE} abm-persona-app.jar
COPY ${JAR_FILE} /app/abm-persona-app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "abm-persona-app.jar"]
