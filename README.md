# ABM Personas - Aplicación Java 17 con Spring Boot 3.2.2

## Introducción:

Este proyecto te permite gestionar personas con una aplicación web completa utilizando Java 17 y Spring Boot 3.2.2.

## Requisitos:

- Docker
- Docker Compose
- Un editor de código (opcional)

# Levantamiento del proyecto:

## Opción 1: Levantamiento completo con Docker Compose:

### Levanta la base de datos, el backend y el frontend:
```
docker-compose up -d
```

## Opción 2: Levantamiento individual (en caso de errores con el backend):

## Levanta la base de datos:
```
docker-compose up -d db
```
## Levanta el backend:

## Opción 2.1: Usando un editor de código:

Abre tu editor de código y navega hasta la carpeta del proyecto.
Ejecuta el comando mvn clean spring-boot:run para iniciar el backend.

## Opción 2.2: Usando Maven:

Abre la terminal y navega hasta la carpeta del proyecto.
Ejecuta el comando mvn clean spring-boot:run para iniciar el backend.

## Solución de problemas:

Si el backend no se levanta con docker-compose up -d, puedes intentar la opción 2 de levantamiento individual.
Si tienes problemas al ejecutar el backend, asegúrate de tener instalado Java 17 y Maven.
Consulta la sección "Contacto" si necesitas ayuda.
Contacto: Ronald Alfonso, raalfonsoparra@gmail.com

Si tienes preguntas o necesitas ayuda, puedes contactarnos a través de tu canal de contacto preferido: inserta tu canal de contacto.

### Agradecimientos:

A la comunidad de Docker y Spring Boot por sus excelentes herramientas.
Esperamos que este proyecto te sea útil.