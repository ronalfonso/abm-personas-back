package com.example.abmpersonaapp.persona.controller;

import com.example.abmpersonaapp.persona.dtos.PersonaDTO;
import com.example.abmpersonaapp.persona.service.PersonaService;
import com.example.abmpersonaapp.tipoDocumento.enums.TipoDocumentoEnum;
import exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/personas")
@RequiredArgsConstructor
public class PersonaController {

    private final PersonaService personaService;

    // **GET /api/personas**
    @GetMapping
    public ResponseEntity<List<PersonaDTO>>getAllPersonas(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "tipoDocumento", required = false) String perTipoDocumento,
            @RequestParam(name = "nombre", required = false) String perNombre) {

        List<PersonaDTO> personas = personaService.findAll(page, size, perTipoDocumento, perNombre);

        return new ResponseEntity<>(personas, HttpStatus.OK);
    }

    // **GET /api/personas/{id}**
    @GetMapping("/{id}")
    public ResponseEntity<Object> findById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(personaService.findById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    // **POST /api/personas**
    @PostMapping
    public ResponseEntity<PersonaDTO> createPersona(@RequestBody PersonaDTO personaDTO) {
        PersonaDTO newPersona = personaService.create(personaDTO);

        return new ResponseEntity<>(newPersona, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updatePersona(@PathVariable Long id, @RequestBody PersonaDTO personaDTO) {
        try {
            return ResponseEntity.ok(personaService.update(id, personaDTO));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        try {
            personaService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/tipos-documento")
    public List<Map<String, Object>> obtenerTiposDocumento() {
        List<Map<String, Object>> tiposDocumento = new ArrayList<>();
        for (TipoDocumentoEnum tipoDocumento : TipoDocumentoEnum.values()) {
            Map<String, Object> tipoDocumentoMap = new HashMap<>();
            tipoDocumentoMap.put("id", tipoDocumento.getId());
            tipoDocumentoMap.put("description", tipoDocumento.getDescription());
            tiposDocumento.add(tipoDocumentoMap);
        }
        return tiposDocumento;
    }
}


