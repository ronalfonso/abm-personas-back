package com.example.abmpersonaapp.persona.mappers;

import com.example.abmpersonaapp.persona.domain.Persona;
import com.example.abmpersonaapp.persona.dtos.PersonaDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonaMapper {

    PersonaDTO toPersonaDTO(Persona persona);

    @InheritInverseConfiguration
    Persona toPersona(PersonaDTO personaDTO);
}
