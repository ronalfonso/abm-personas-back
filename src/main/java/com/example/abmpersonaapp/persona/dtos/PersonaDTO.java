package com.example.abmpersonaapp.persona.dtos;

import com.example.abmpersonaapp.tipoDocumento.enums.TipoDocumentoEnum;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PersonaDTO {

    private Long id;

    private String perNombre;

    private String perApellido;

    private TipoDocumentoEnum perTipoDocumento;

    private Long perNumeroDocumento;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date perFechaNacimiento;
}
