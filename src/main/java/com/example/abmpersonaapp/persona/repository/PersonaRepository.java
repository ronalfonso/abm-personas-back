package com.example.abmpersonaapp.persona.repository;

import com.example.abmpersonaapp.persona.domain.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {
    Page<Persona> findAll(Specification<Persona> specification, Pageable pageable);
}
