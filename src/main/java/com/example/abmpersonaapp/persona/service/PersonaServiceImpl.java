package com.example.abmpersonaapp.persona.service;

import com.example.abmpersonaapp.persona.domain.Persona;
import com.example.abmpersonaapp.persona.dtos.PersonaDTO;
import com.example.abmpersonaapp.persona.mappers.PersonaMapper;
import com.example.abmpersonaapp.persona.repository.PersonaRepository;
import exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.abmpersonaapp.tipoDocumento.enums.TipoDocumentoEnum.getTipoDocumentoValido;

@Slf4j
@Service
@RequiredArgsConstructor
@Validated
public class PersonaServiceImpl implements PersonaService {


    private final PersonaRepository personaRepository;

    private final PersonaMapper personaMapper;

    public List<PersonaDTO> findAll(@RequestParam(name = "page", defaultValue = "0") int page,
                                    @RequestParam(name = "size", defaultValue = "10") int size,
                                    @RequestParam(name = "tipoDocumento", required = false) String perTipoDocumento,
                                    @RequestParam(name = "nombre", required = false) String perNombre) {

        Pageable pageable = PageRequest.of(page, size);

        Specification<Persona> specification = null;
        if (perTipoDocumento != null) {
            specification = Specification.where((persona, criteria, cb) -> cb.equal(persona.get("perTipoDocumento"), perTipoDocumento));
        }
        if (perNombre != null) {
            if (specification != null) {
                specification = specification.and((persona, criteria, cb) -> cb.like(persona.get("perNombre"), "%" + perNombre + "%"));
            } else {
                specification = Specification.where((persona, criteria, cb) -> cb.like(persona.get("perNombre"), "%" + perNombre + "%"));
            }
        }
        Page<Persona> personas = personaRepository.findAll(specification, pageable);

        return personas.getContent().stream()
                .map(personaMapper::toPersonaDTO)
                .collect(Collectors.toList());
    }

    public PersonaDTO findById(Long id) {
        Persona persona = getPersona(id);
        personaNoExist(id, persona);
        return personaMapper.toPersonaDTO(persona);
    }

    @Override
    public PersonaDTO create(PersonaDTO personaDTO) {
        return personaMapper.toPersonaDTO(save(personaMapper.toPersona(personaDTO)));
    }

    public PersonaDTO update(Long id, PersonaDTO personaDTO) {
        Persona persona = getPersona(id);
        personaNoExist(id, persona);

        persona.setPerNombre(personaDTO.getPerNombre());
        persona.setPerApellido(personaDTO.getPerApellido());
        persona.setPerTipoDocumento(getTipoDocumentoValido(personaDTO.getPerTipoDocumento().getDescription()));
        persona.setPerNumeroDocumento(personaDTO.getPerNumeroDocumento());
        persona.setPerFechaNacimiento(personaDTO.getPerFechaNacimiento());
        return personaMapper.toPersonaDTO(save(persona));
    }

    public void delete(Long id) {
        Persona persona = getPersona(id);
        personaNoExist(id, persona);
        personaRepository.deleteById(id);
    }

    private Persona getPersona(Long id) {
        return personaRepository.findById(id).orElse(null);
    }

    private Persona save(Persona persona) {
        return personaRepository.save(persona);
    }

    private static void personaNoExist(Long id, Persona persona) {
        if (persona == null) {
            throw new ResourceNotFoundException("No se encontro el recurso con id: " + id);
        }
    }
}
