package com.example.abmpersonaapp.persona.service;

import com.example.abmpersonaapp.persona.dtos.PersonaDTO;

import java.util.List;

public interface PersonaService {

    PersonaDTO findById(Long id);

    List<PersonaDTO> findAll(int page, int size, String perTipoDocumento, String perNombre);

    PersonaDTO create(PersonaDTO personaDTO);

    PersonaDTO update(Long id, PersonaDTO personaDTO);

    void delete(Long id);
}
