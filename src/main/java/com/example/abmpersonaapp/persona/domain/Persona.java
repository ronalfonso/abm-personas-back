package com.example.abmpersonaapp.persona.domain;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "personas")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false)
    @NotBlank
    private String perNombre;

    @Column(name = "apellido", nullable = false)
    @NotBlank
    private String perApellido;

    @Column(name = "tipo_documento", nullable = false)
    @NotBlank
    private String perTipoDocumento;

    @Column(name = "numero_documento", nullable = false)
    @NotBlank
    private Long perNumeroDocumento;

    @Column(name = "fecha_nacimiento", nullable = false)
    @NotBlank
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date perFechaNacimiento;




}
