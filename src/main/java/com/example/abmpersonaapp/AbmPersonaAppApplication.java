package com.example.abmpersonaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.example.abmpersonaapp.*"})
@EntityScan(basePackages = {"com.example.abmpersonaapp.*"})
public class AbmPersonaAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbmPersonaAppApplication.class, args);
    }
}
