package com.example.abmpersonaapp.tipoDocumento.enums;

import lombok.Getter;

@Getter
public enum TipoDocumentoEnum {
    DNI(1, "DNI"),
    PASAPORTE(2, "PASAPORTE"),
    CEDULA(3, "CEDULA");

    private final Integer id;
    private final String description;

    TipoDocumentoEnum(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public static String getTipoDocumentoValido(String tipoDocumento) {
        for (TipoDocumentoEnum tipo : TipoDocumentoEnum.values()) {
            if (tipo.getDescription().equals(tipoDocumento)) {
                return tipo.getDescription();
            }
        }
        throw new IllegalArgumentException("El tipo de documento no es válido: " + tipoDocumento);
    }

}
