package com.example.abmpersonaapp.tipoDocumento.domain;

import com.example.abmpersonaapp.tipoDocumento.enums.TipoDocumentoEnum;
import jakarta.persistence.*;

@Entity
@Table(name = "tipo_documentos")
public class TipoDocumento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String descripcion;

    @Column(name = "tipo", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoDocumentoEnum tipo;
}
